const postsLink = "https://ajax.test-danit.com/api/json/posts";
const url = 'https://ajax.test-danit.com/api/json/'

function createElement(elementName, className, text){
    const element = document.createElement(elementName);
    element.className = className;
    if (text) { element.innerText = text; }
    return element;
}

class Card {
    constructor(title,body,userId,id){
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.id = id;
    }
    async createCard(){
        const userLink = `https://ajax.test-danit.com/api/json/users/${this.userId}`
        const userLinkRequest = fetch(userLink).then(response => response.json());

        const deleteBtn = createElement("span", "delete-btn", "X")
        deleteBtn.addEventListener("click", (event)=>{
            fetch(`${postsLink}/${this.id}`, {
                method: 'DELETE',
            });
            event.target.closest(".card").remove();
        })

        const card = createElement("div", "card");
        const name = createElement('p',"card-name", `Name : ${await userLinkRequest.then(user => user.name)}`);
        const email = createElement("p", "card-email", `Email : ${await userLinkRequest.then(user => user.email)}`);
        const title = createElement('h2', "card-title", `${this.title}`);
        const text = createElement('p', "card-text", `${this.body}`);
        card.append( deleteBtn,title,name,email, text);
        document.querySelector('.post-board').prepend(card);

    }
}
async function showCards(){
    const postResponse = await fetch(postsLink);
    const posts = await postResponse.json();

    posts.forEach( async post => {
        const {title, body, userId, id} = post;
        const card = new Card(title, body, userId, id)
        card.createCard()
    })
}
showCards()